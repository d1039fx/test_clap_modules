import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:user_widget_package/bloc/adaptative/adaptative_bloc.dart';

import '../../bloc/data/data_bloc.dart';

class UserData extends StatelessWidget {
  const UserData({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AdaptativeBloc, AdaptativeState>(
      builder: (context, state) {
        double screenWidth = state.boxConstraints.maxWidth;

        return BlocBuilder<DataBloc, DataState>(
          builder: (context, data) {
            return Container(
              margin: const EdgeInsets.symmetric(horizontal: 20),
              child: Stack(
                alignment: Alignment.center,
                children: [
                  AnimatedPositioned(
                    duration: const Duration(milliseconds: 250),
                    height: 70,
                    top: 0,
                    left: state.left == 0 ? 0 : null,
                    child: const CircleAvatar(
                      radius: 35,
                      backgroundColor: Colors.teal,
                      child: CircleAvatar(
                          backgroundColor: Colors.white,
                          radius: 29,
                          child: Icon(Icons.person)),
                    ),
                  ),
                  AnimatedPositioned(
                    duration: const Duration(milliseconds: 250),
                    top: state.top,
                    height: 60,
                    left: state.left == 0 ? 80 : null,
                    width: screenWidth / 3,
                    child: Column(
                      crossAxisAlignment: state.left == 0
                          ? CrossAxisAlignment.start
                          : CrossAxisAlignment.center,
                      children: [
                        Text(
                          data.inputData['name'],
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 18),
                        ),
                        Text(
                          data.inputData['last_name'],
                          style: const TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 18),
                        )
                      ],
                    ),
                  ),
                  AnimatedPositioned(
                      top: state.top == 0 ? 55 : 120,
                      left: state.left == 0 ? 90 : null,
                      duration: const Duration(milliseconds: 250),
                      child: Text(
                        data.inputData['type'],
                        style: const TextStyle(color: Colors.blue),
                      )),
                  AnimatedPositioned(
                    duration: const Duration(milliseconds: 250),
                    top: state.top == 0 ? 90 : 140,
                    left: state.left == 0 ? 80 : null,
                    child: Row(
                      children: [
                        const Icon(Icons.email),
                        const SizedBox(
                          width: 10,
                        ),
                        Text(data.inputData['email'])
                      ],
                    ),
                  )
                ],
              ),
            );
          },
        );
      },
    );
  }
}

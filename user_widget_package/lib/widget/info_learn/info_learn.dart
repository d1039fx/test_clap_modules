import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:user_widget_package/bloc/adaptative/adaptative_bloc.dart';
import 'package:user_widget_package/bloc/adaptative/adaptative_bloc.dart';
import 'package:user_widget_package/widget/info_learn/widget/category_info_learn.dart';

import '../../bloc/data/data_bloc.dart';

class InfoLearn extends StatelessWidget {
  const InfoLearn({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AdaptativeBloc, AdaptativeState>(
      builder: (context, state) {
        return BlocBuilder<DataBloc, DataState>(
          builder: (context, data) {
            return Container(
              decoration: BoxDecoration(
                  color:
                      state.left == 0 ? Colors.grey[200] : Colors.transparent,
                  borderRadius: BorderRadius.circular(10)),
              margin: const EdgeInsets.symmetric(horizontal: 15),
              child: Row(
                children: [
                  Expanded(
                    child: CategoryInfoLearn(
                        name: 'Cursos completados',
                        rangeValue: data.inputData['courses']['total'],
                        currentValue: data.inputData['courses']['current']),
                  ),
                  Expanded(
                    child: CategoryInfoLearn(
                        name: 'Cursos sincrónicos y blended',
                        rangeValue: data.inputData['synchronous_courses']['total'],
                        currentValue: data.inputData['synchronous_courses']['current']),
                  ),
                  Expanded(
                    child: CategoryInfoLearn(
                        name: state.fontSize >= 12
                            ? 'Ruta de aprendizaje'
                            : 'Rutas',
                        rangeValue: data.inputData['learning_path']['total'],
                        currentValue: data.inputData['learning_path']['current']),
                  ),
                   Expanded(
                    child: CategoryInfoLearn(
                        name: 'Certificaciones',
                        rangeValue: data.inputData['certifications']['total'],
                        currentValue: data.inputData['certifications']['current']),
                  ),
                ],
              ),
            );
          },
        );
      },
    );
  }
}

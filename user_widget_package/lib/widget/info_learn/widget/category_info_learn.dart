import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:user_widget_package/bloc/adaptative/adaptative_bloc.dart';

class CategoryInfoLearn extends StatelessWidget {
  final String name, rangeValue, currentValue;

  const CategoryInfoLearn(
      {Key? key,
      required this.name,
      required this.rangeValue,
      required this.currentValue})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AdaptativeBloc, AdaptativeState>(
      builder: (context, state) {
        return Container(
          decoration: BoxDecoration(
              color: state.left == 0 ? Colors.transparent : Colors.white,
              border: Border.all(
                  color:
                      state.left == 0 ? Colors.transparent : Colors.grey[300]!,
                  width: 3),
              borderRadius: BorderRadius.circular(10)),
          margin: const EdgeInsets.symmetric(horizontal: 5),
          height: state.left == 0 ? 85 : 140,
          width: state.left == 0 ? 85 : 140,
          child: Column(
            children: [
              Expanded(
                flex: state.fontSize >= 9 ? 1 : 2,
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Text(
                    name,
                    style: TextStyle(
                        color: Colors.grey[600],
                        fontWeight: FontWeight.w600,
                        fontSize: state.fontSize),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              Divider(
                height: 20,
                color: state.left == 0 ? Colors.transparent : Colors.black,
                thickness: 2,
                endIndent: 50,
                indent: 50,
              ),
              Expanded(
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Text(
                    '$currentValue/$rangeValue',
                    style: TextStyle(
                        color: Colors.blue[800],
                        fontSize: state.left == 0 ? 18 : 25,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

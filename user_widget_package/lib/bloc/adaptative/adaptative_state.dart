part of 'adaptative_bloc.dart';

abstract class AdaptativeState extends Equatable {
  final BoxConstraints boxConstraints;
  final double left, top, right, fontSize;

  const AdaptativeState(
      {required this.boxConstraints,
        required this.fontSize,
      required this.left,
      required this.top,
      required this.right});
}

class AdaptativeInitial extends AdaptativeState {
  const AdaptativeInitial(
      {required super.boxConstraints,
      required super.left,
  required super.fontSize,
      required super.top,
      required super.right});

  @override
  List<Object> get props => [boxConstraints, left, top, right];
}

class GetAdaptative extends AdaptativeState {
  const GetAdaptative(
      {required super.boxConstraints,
      required super.left,
  required super.fontSize,
      required super.top,
      required super.right});
  @override
  // TODO: implement props
  List<Object?> get props => [boxConstraints, left, top, right];
}

part of 'adaptative_bloc.dart';

abstract class AdaptativeEvent extends Equatable {

  final BoxConstraints boxConstraints;

  const AdaptativeEvent({required this.boxConstraints});
}

class SetAdaptative extends AdaptativeEvent{
  const SetAdaptative({required super.boxConstraints});

  @override
  // TODO: implement props
  List<Object?> get props => [boxConstraints];

}
import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'adaptative_event.dart';
part 'adaptative_state.dart';

class AdaptativeBloc extends Bloc<AdaptativeEvent, AdaptativeState> {
  final BoxConstraints boxConstraints;
  double fontSize = 9;

  AdaptativeBloc(this.boxConstraints)
      : super(AdaptativeInitial(
            boxConstraints: boxConstraints, left: 0, top: 0, right: 0,fontSize: 11)) {
    on<SetAdaptative>((event, emit) {
      double left = event.boxConstraints.maxWidth >= 820 ? 100 : 0;
      double top = event.boxConstraints.maxWidth >= 820 ? 70 : 0;
      double right = event.boxConstraints.maxWidth >= 820 ? 20 : 0;

      if(event.boxConstraints.maxWidth >= 820){
        fontSize = 12;
      }else{
        fontSize = 9;
      }
      if(event.boxConstraints.maxWidth >= 1280){
        fontSize = 17;
      }
      emit(GetAdaptative(
          boxConstraints: event.boxConstraints,
          left: left,
          top: top,
          right: right, fontSize: fontSize));
    });
  }
}

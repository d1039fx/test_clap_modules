import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'data_event.dart';
part 'data_state.dart';

class DataBloc extends Bloc<DataEvent, DataState> {
  final Map<String, dynamic> dataInput;

  DataBloc({required this.dataInput})
      : super(DataInitial(
          inputData: dataInput,
        )) {
    on<DataEvent>((event, emit) {
      emit(DataIn(
        inputData: dataInput,
      ));
    });
  }
}

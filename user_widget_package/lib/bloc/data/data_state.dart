part of 'data_bloc.dart';

abstract class DataState extends Equatable {
  final Map<String, dynamic> inputData;
  const DataState({required this.inputData});
}

class DataInitial extends DataState {
  const DataInitial({
    required super.inputData,
  });

  @override
  List<Object> get props => [
        inputData,
      ];
}

class DataIn extends DataState {
  const DataIn({required super.inputData});

  @override
  List<Object> get props => [
        inputData,
      ];
}

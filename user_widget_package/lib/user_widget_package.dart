library user_widget_package;

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:user_widget_package/bloc/adaptative/adaptative_bloc.dart';
import 'package:user_widget_package/bloc/data/data_bloc.dart';
import 'package:user_widget_package/report_widget.dart';
import 'package:user_widget_package/widget/user_data/user_data.dart';

/// A Calculator.
class Calculator {
  /// Returns [value] plus 1.
  int addOne(int value) => value + 1;
}

class UserWidgetPackage extends StatelessWidget {
  final Map<String, dynamic> data;
  final ValueChanged<Map<String, dynamic>> dataWidget;

  const UserWidgetPackage(
      {Key? key, required this.data, required this.dataWidget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, size) {
      return MultiBlocProvider(
          providers: [
            BlocProvider<AdaptativeBloc>(
                create: (context) => AdaptativeBloc(size)),
            BlocProvider<DataBloc>(
                create: (context) => DataBloc(dataInput: data)),
          ],
          child: LayoutBuilder(builder: (context, size) {
            context
                .read<AdaptativeBloc>()
                .add(SetAdaptative(boxConstraints: size));
            return ReportWidget(
              dataOut: (data) {
                dataWidget.call(data);
              },
            );
          }));
    });
  }
}

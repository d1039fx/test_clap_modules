import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:user_widget_package/bloc/adaptative/adaptative_bloc.dart';
import 'package:user_widget_package/widget/info_learn/info_learn.dart';
import 'package:user_widget_package/widget/user_data/user_data.dart';

import 'bloc/data/data_bloc.dart';

class ReportWidget extends StatelessWidget {
  final ValueChanged<Map<String, dynamic>> dataOut;

  const ReportWidget({Key? key, required this.dataOut}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AdaptativeBloc, AdaptativeState>(
      builder: (context, state) {
        double screenWidth = state.boxConstraints.maxWidth;
        return Stack(
          children: [
            AnimatedPositioned(
                height: state.left == 0 ? 150 : 210,
                width: screenWidth,
                duration: const Duration(),
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  decoration: BoxDecoration(
                      color: state.left == 0 ? Colors.white : Colors.grey[200],
                      borderRadius: BorderRadius.circular(10)),
                )),
            AnimatedPositioned(
                top: 20,
                height: state.left == 0 ? 150 : 210,
                width: state.left == 0 ? screenWidth : screenWidth / 3,
                duration: const Duration(milliseconds: 250),
                child:
                SizedBox(width: screenWidth / 3, child: const UserData())),
            AnimatedPositioned(
                top: state.top == 0 ? 140 : state.top + 30,
                height: 140,
                width: state.right == 0 ? screenWidth : screenWidth / 1.5,
                right: state.right,
                duration: const Duration(milliseconds: 250),
                child: const InfoLearn()),
            AnimatedPositioned(
                top: 30,
                height: 40,
                width: 160,
                right: state.left == 0 ? 0 : state.right + 20,
                duration: const Duration(milliseconds: 250),
                child: state.left == 0
                    ? const SizedBox() : BlocBuilder<DataBloc, DataState>(
                  builder: (context, data) {
                    return ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.blue[800],
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                      ),
                      onPressed: () {
                        dataOut.call(data.inputData);
                      },
                      child: const Text('Descargar informe',
                          style: TextStyle(color: Colors.white)),
                    );
                  },
                ))
          ],
        );
      },
    );
  }
}
